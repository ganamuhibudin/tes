// ojo lali di inject ui bootstrap e
var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap']);
app.directive('paginatebrada', function(){
    return {
        restrict : 'E',
        templateUrl : site_url + 'app/dirPagination.tpl.html'
    }
});

app.factory("services", ['$http', function($http) {
  var serviceBase = site_url;
    var obj = {};
    obj.getCustomers = function(){
        return $http.get(serviceBase + 'table/customers');
    }
    obj.getCustomer = function(customerID){
        return $http.get(serviceBase + 'table/getCustomer/' + customerID);
    }

    obj.insertCustomer = function (customer) {
        return $http.post(serviceBase + 'table/insertCustomer', customer).then(function (results) {
            return results;
        });
    };

    obj.updateCustomer = function (id,customer) {
        return $http.post(serviceBase + 'table/updateCustomer', {id:id, customer:customer}).then(function (status) {
            return status.data;
        });
    };

    obj.deleteCustomer = function (id) {
        return $http.delete(serviceBase + 'table/deleteCustomer/' + id).then(function (status) {
            return status.data;
        });
    };

    return obj;   
}]);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.controller('listCtrl', function ($scope, services, $timeout) {
    services.getCustomers().then(function(data){
        $scope.customers = data.data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 5; //max no of items to display in a page
        $scope.filteredItems = $scope.customers.length; //Initially for no filter
        $scope.totalItems = $scope.customers.length;
    });

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
        console.log('going to page ' + $scope.currentPage);
    };

    $scope.filter = function() {
        $timeout(function() {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };

    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

app.controller('editCtrl', function ($scope, $rootScope, $location, $routeParams, services, customer) {
    var customerID = ($routeParams.customerID) ? parseInt($routeParams.customerID) : 0;
    $rootScope.title = (customerID > 0) ? 'Edit Customer' : 'Add Customer';
    $scope.buttonText = (customerID > 0) ? 'Update Customer' : 'Add New Customer';
    
    if(customerID > 0) {
      var original = customer.data[0];
      // original._id = customerID;
      $scope.customer = angular.copy(original);
      $scope._id = customerID;

      $scope.isClean = function() {
        return angular.equals(original, $scope.customer);
      }

      $scope.deleteCustomer = function(customer) {
        $location.path('/');
        if(confirm("Are you sure to delete customer number: "+ $scope._id)==true){
            services.deleteCustomer($scope._id);
        }
      }
    }
      $scope.saveCustomer = function(customer) {
        //alert(customer); return false;
        //console.log(customer);
        $location.path('/');
        if (customerID <= 0) {
            services.insertCustomer(customer);

        }
        else {
            services.updateCustomer(customerID, customer);
        }
    };
});
app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        title: 'Customers',
        templateUrl: site_url + 'table/loadview/customers',
        controller: 'listCtrl'
      })
      .when('/edit-customer/:customerID', {
        title: 'Edit Customers',
        templateUrl: site_url + 'table/loadview/edit-customer',
        controller: 'editCtrl',
        resolve: {
          customer: function(services, $route){
            var customerID = $route.current.params.customerID;
            return services.getCustomer(customerID);
          }
        }
      })
      .otherwise({
        redirectTo: '/'
      });
}]);
app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);
