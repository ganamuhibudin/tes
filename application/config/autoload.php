<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$autoload['libraries'] = array('database', 'parser', 'newsession');
$autoload['packages'] = array();
$autoload['helper'] = array('url','file','download','email','cookie','array','form');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();