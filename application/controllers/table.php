<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Table extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('table_act');
	}

	public function index(){
		$this->load->view('table');
	}

	public function loadview($view){
		$this->load->view('partials/'.$view);
	}

	public function customers(){
		$data = $this->table_act->customers();
		echo json_encode($data);
	}

	public function getCustomer($id){
		$data = $this->table_act->getCustomer($id);
		//print_r($data);
		echo json_encode($data);
	}

	public function insertCustomer(){
		$post = json_decode(trim(file_get_contents('php://input')), true);
		$data = $this->table_act->insertCustomer($post);
		echo json_encode($data);
	}

	public function updateCustomer(){
		$post = json_decode(trim(file_get_contents('php://input')), true);
		//print_r($post);
		$data = $this->table_act->updateCustomer($post);
		echo json_encode($data);	
	}

	public function deleteCustomer($id){
		$delete = $this->table_act->deleteCustomer($id);
		echo json_encode($delete);
	}

}