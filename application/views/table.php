<!DOCTYPE html>
<html ng-app="myApp" ng-app lang="en">
<head>
    <meta charset="utf-8">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        ul>li, a{cursor: pointer;}
    </style>
    <title>Creating a Simple RESTful PHP web service which is consumed by a AngularJS application</title>
</head>
<body>
<div class="navbar navbar-default" id="navbar">
    <div class="container" id="navbar-container">
    <div class="navbar-header">
        <a href="#" class="navbar-brand">
            <small>
                <i class="glyphicon glyphicon-log-out"></i>
                AngularCode / AngularJS Demos 
            </small>
        </a><!-- /.brand -->
        
    </div><!-- /.navbar-header -->
    </div>
</div>

<div class="container">
<br/>
<blockquote><h4><a href="#">A simple demonstration of CRUD RESTful php service that can be used with Angularjs & mysql</a></h4></blockquote>
<br/>

    <br/>
    <div ng-view="" id="ng-view"></div>

</div>

<script src="<?= base_url(); ?>assets/js/angular.min.js"></script>
<script src="<?= base_url(); ?>assets/js/angular-route.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var site_url = '<?php echo site_url(); ?>';
</script>
<script src="<?= base_url(); ?>app/app.js"></script>             
</body>
</html>