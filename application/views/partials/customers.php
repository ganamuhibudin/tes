<div ng-controller="listCtrl">
    <div class="row">
        <div class="col-md-2">PageSize:
            <select ng-model="entryLimit" class="form-control">
                <option>5</option>
                <option>10</option>
                <option>20</option>
                <option>50</option>
                <option>100</option>
            </select>
        </div>
        <div class="col-md-3">Filter:
            <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
        </div>
        <div class="col-md-4">
            <h5>Filtered {{filtered.length}} of {{totalItems}} total customers</h5>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12" ng-show="customers.length > 0">
            <nav class= "navbar navbar-default" role= "navigation" >
                <div class= "navbar-header" >
                 <a class="btn btn-lg btn-success" href="#/edit-customer/0"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add new Customer</a>
                </div>
            </nav>
        </div>
        <div class="col-md-12" ng-show="filteredItems > 0">
            <table class="table table-striped table-bordered">
            <thead>
                <th>Customer Name&nbsp;</th>
                <th>Email&nbsp;</th>
                <th>Address&nbsp;</th>
                <th>City&nbsp;</th>
                <th>Country&nbsp;</th>
                <th>Action&nbsp;</th>
            </thead>
            <tbody>
                <tr ng-repeat="data in filtered = (customers | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                    <td>{{data.customerName}}</td>
                    <td>{{data.email}}</td>
                    <td>{{data.address}}</td>
                    <td>{{data.city}}</td>
                    <td>{{data.country}}</td>
                    <td><a href="#/edit-customer/{{data.customerNumber}}" class="btn">&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp; Edit Customer</a></td>
                </tr>
            </tbody>
            </table>
        </div>
        <div class="col-md-12" ng-show="filteredItems == 0">
            <div class="col-md-12">
                <h4>No customers found</h4>
            </div>
        </div>
        <!-- 
            ternyata pagination e gawe wek an e angular ui bootstrap
            dadi tak donlot ae angular ui bootstrap sing versi terbaru, ene nang asset/js
            terus pagination sing iki gawe sing versi anyar e.

            demo karo option e ono nang kene :
            http://angular-ui.github.io/bootstrap/#/pagination
        -->
        <div class="col-md-12" ng-show="filteredItems > 0">
            <pagination ng-change="pageChanged()" total-items="filteredItems" items-per-page="entryLimit" ng-model="currentPage" max-size="maxSize" class="pagination-sm" boundary-links="true" rotate="false" num-pages="numPages"></pagination>
        </div>
    </div>
</div>