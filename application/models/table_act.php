<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Table_act extends CI_Model {

	function customers(){
		$query="SELECT distinct c.customerNumber, c.customerName, c.email, c.address, c.city, c.state, c.postalCode, c.country FROM angularcode_customers c order by c.customerNumber desc";
		$data = $this->db->query($query);
		if($data->num_rows() > 0){
			$rtn = $data->result_array();
		}
		return $rtn;
	}

	function getCustomer($id){
		$query="SELECT distinct c.customerName, c.email, c.address, c.city, c.state, c.postalCode, c.country FROM angularcode_customers c where c.customerNumber='".$id."'";
		$data = $this->db->query($query);
		if($data->num_rows() > 0){
			$rtn = $data->result_array();
		}else{
			$rtn = '';
		}
		return $rtn;
	}

	function insertCustomer($post){
		$exec = $this->db->insert('angularcode_customers', $post);
		if($exec){
			$success = array('status' => "Success", "msg" => "Customer Created Successfully.", "data" => $post);
			return $success;
		}
	}

	function updateCustomer($post){
		$id = $post['id'];
		//unset($post['customer']['_id']);
		if($id){
			$this->db->where('customerNumber', $id);
			$this->db->update('angularcode_customers', $post['customer']);
			$success = array('status' => "Success", "msg" => "Customer ".$id." Updated Successfully.", "data" => $post);
			return $success;
		}
	}

	function deleteCustomer($id){
		$exec = $this->db->delete('angularcode_customers', array('customerNumber'=> $id));
		if($exec){
			$success = array('status' => "Success", "msg" => "Successfully deleted one record.");
			return $success;
		}
	}
}